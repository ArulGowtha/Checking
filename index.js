/**
 * @format
 */

import { AppRegistry } from "react-native";
import { name as appName } from "./app.json";
import Gallery from "./src/screens/Gallery";
import Drop from "./src/screens/Drop";

AppRegistry.registerComponent(appName, () => Drop);
